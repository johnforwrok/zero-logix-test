<?php

namespace App\Services;

use Stripe\StripeClient;

class StripeService
{
    protected $client;

    public function __construct(StripeClient $client)
    {
        $this->client = $client;
    }

    public function createPayment($params)
    {
        $data = [
            'success_url' => config('app.url') . 'success',
            'line_items' => [
                [

                    'price' => $params['price'],
                    'quantity' => $params['quantity'],
                ]
            ],
            'mode' => 'payment'
        ];
        $response = $this->client->checkout->sessions->create($data);

        return $response;
    }

    public function queryPayment($params)
    {
        # code...
    }

    public function createProduct($params)
    {
        # code...
    }

    public function getProduct($params)
    {
        # code...
    }

    public function createPrice($params)
    {
        # code...
    }

    public function getPrice($params)
    {
        # code...
    }
}
