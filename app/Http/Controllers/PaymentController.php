<?php

namespace App\Http\Controllers;

use Stripe\Stripe;
use Stripe\StripeClient;
use Illuminate\Http\Request;
use Stripe\Checkout\Session;
use App\Services\StripeService;

class PaymentController extends Controller
{
    protected $client;

    protected $stripeService;

    public function __construct(StripeClient $client, StripeService $stripeService)
    {
        $this->client = $client;
        $this->stripeService = $stripeService;
    }

    public function createProduct(Request $request)
    {
        $response = $this->client->products->create([
            'name' => 'iphone 15 pro'
        ]);

        return $response;
    }

    public function getProduct()
    {
        $response = $this->client->products->retrieve('prod_OuIUq0KQUBAlTI');

        return $response;
    }

    public function createPrice(Request $request)
    {
        $params = $request->all();
        $response = $this->client->prices->create([
            'unit_amount' => $params['unit_amount'],
            'currency' => $params['currency'],
            'product' => $params['product'],
        ]);

        return $response;
    }

    public function getPrice()
    {
        $response = $this->client->prices->retrieve('price_1O6UDqBiXgImlwgFiaI1muzu');

        return $response;
    }

    public function paymentOrder(Request $request)
    {
        $params = $request->all();
        $response = $this->stripeService->createPayment($params);
        $result = $response->getLastResponse()->json;

        logger('payment response', $result);

        return header('Location:' . $response->url);
    }

    function success(Request $request)
    {
        logger('payment success response', $request->all());
    }

    /**
     * query order
     *
     * @param Request $request
     */
    public function queryOrder(Request $request)
    {
        $params = $request->all();

        $response = $this->client->checkout->sessions->retrieve($params['id']);

        $result = $response->getLastResponse()->json;

        logger('query response', $result);
    }

    public function payment2()
    {
        Stripe::setApiKey(config('stripe.secret'));

        $respone = Session::create([
            'cancel_url' => 'https://stripe.com/cancel',
            'client_reference_id' => '1234',
            'line_items' => [
                [
                    'price_data' => [
                        'unit_amount' => 123,
                        'currency' => 'usd',
                        'product_data' => [
                            'name' => 'name',
                            'description' => 'item 1',
                            'images' => [
                                'https://stripe.com/img1',
                            ],
                        ],
                    ],
                    'quantity' => 2,
                ],
            ],
            'payment_intent_data' => [
                'receipt_email' => 'test@stripe.com',
            ],
            'mode' => 'payment',
            'success_url' => 'https://stripe.com/success',
        ]);

        header('location:' . $respone->url);
    }
}
