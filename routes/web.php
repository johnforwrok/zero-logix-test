<?php

use App\Http\Controllers\PaymentController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('create-product', [PaymentController::class, 'createProduct']);
Route::get('get-product', [PaymentController::class, 'getProduct']);

Route::get('create-price', [PaymentController::class, 'createPrice']);
Route::get('get-price', [PaymentController::class, 'getPrice']);

Route::get('payment', [PaymentController::class, 'paymentOrder']);
Route::get('query', [PaymentController::class, 'queryOrder']);

Route::any('success', [PaymentController::class, 'success']);



Route::get('payment2', [PaymentController::class,'payment2']);

