<?php

namespace Tests\Feature;

use Tests\TestCase;
use Stripe\StripeClient;
use Stripe\Checkout\Session;
use App\Services\StripeService;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Config;

class StripeServiceTest extends TestCase
{
    public function setUp(): void
    {
        app()->bind(StripeClient::class, function() { // not a service provider but the target of service provider
            return new StripeClient(Config::set('stripe.secret', 'test'));
        });
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_create_payment()
    {
        $service = app(StripeService::class);

        $response = $service->createPayment([
            'price' => 'price_payment_1234',
            'quantity' => 1,
        ]);

        $this->assertInstanceOf(Session::class, $response);
    }
}
